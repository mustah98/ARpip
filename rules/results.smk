import os
import csv
import pandas as pd
import matplotlib as mp
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from pathlib import Path
import warnings
import sys




def amr_tool():
	if config["amr_tool"]["amr_finder_plus"] == True:
		return(expand("results/amr_finder_plots/{Dataset}.png", Dataset = DATASET))
	else:
		return 'results/none.txt'

def input():

	if config["amr_tool"]["amr_finder_plus"] == True:
		return (os.path.abspath(os.getcwd()) + "/results/amrfinder/")
	else:
		return 'results/none.txt'

rule plot_amrfinder:
	input:
		expand("results/amrfinder/{sample}.tsv", sample = list(samples.index)),
	output:
		amr_tool(),
		"results/amrfinder_presence_absence.csv",
	params:
		path = input()
	run:

		if not sys.warnoptions:
			warnings.simplefilter("ignore")

		if config["amr_tool"]["amr_finder_plus"] == True:
			print("Using AMRfinder plus and Abricate..")
			# mr = multi resistance


			def colors_from_values(values, palette_name):
				normalized = (values - min(values)) / (max(values) - min(values))
				indices = np.round(normalized * (len(values) - 1)).astype(np.int32)
				palette = sns.color_palette(palette_name, len(values))
				return np.array(palette).take(indices, axis=0)

# --------------------------------------------------------------------------------------------------------------------------------------------------


			Path(os.path.abspath(os.getcwd()) + "/results/amr_finder_plots/").mkdir(parents = True, exist_ok = True)
			path = params.path
			data = sorted(os.listdir(path))
			global_gens = []
			gen_counter = []
			name = [file.split('.')[0] for file in data]
			counter_mr_isolates = 0
			def find_global_genes(data, path, global_gens):
				print("\nfinding global genes..")
				counter_mr_isolates = 0
				isolate_w_max_mr= 0
				i = 0
				for elem in data:
					file = open(path+elem)
					print(elem)
					df = pd.read_csv(file, delimiter = "\t")
					for rows in df["Gene symbol"]:
						if rows not in global_gens:
							global_gens.append(rows)
					i = i + 1
					file.close()
				return(global_gens)

			find_global_genes(data, path, global_gens)

			w= len(global_gens)
			h= len(data)
			Matrix = [[0 for i in range(w)] for j in range(h)]


			def find_gene(Matrix, data, path, global_gens):
				print("\ngenerate gene presence absence Matrix..")
				pos_strain = 0
				for i in range(len(data)):
					file = open(path+data[i])
					df = pd.read_csv(file, delimiter = "\t", encoding = "uft-8")
					for gene in df["Gene symbol"]:
						index = global_gens.index(gene)
						Matrix[i][index] = 1
					file.close()
				return Matrix

			find_gene(Matrix, data, path, global_gens)

			def get_num_of_gen(w,h, gen_counter, Matrix):
				print("\ngenerating gene count Matrix..")
				for i in range(w):
					counter = 0
					for j in range(h):
						if Matrix[j][i] == 1:
							counter += 1
						else:
							continue
					gen_counter.append(counter)
				return gen_counter

			get_num_of_gen(w, h, gen_counter, Matrix)


			def plots(Matrix, gen_counter):
				sns.set(style="whitegrid", color_codes=True)
				print("\nplotting Heatmap..")
				abs_path = os.path.abspath(os.getcwd())
				plt.subplots(figsize=(20,30))
				sns.heatmap(Matrix, cmap="viridis",  cbar = False )
				plt.savefig(abs_path + "/results/amr_finder_plots/AB_genes_heatmap_amrfinder.png", dpi=300)
				plt.close()

				print("\nplotting Bar Plot..")
				y_pos = np.arange(len(global_gens))
				# plt.figure(figsize = (12,12))
				# plt.bar(y_pos, gen_counter, align = 'center', alpha = 0.5, color=color_map(data_normalizer(gen_counter)))
				plt.subplots(figsize=(12,12))
				plots = sns.barplot(x = y_pos, y = gen_counter, palette = colors_from_values(np.array(gen_counter), "YlOrRd"))
				for bar in plots.patches:
					plots.annotate(format(bar.get_height(), '.2f'),
					(bar.get_x() + bar.get_width() / 2,
					bar.get_height()), ha='center', va='center',
					size=15, xytext=(0, 5),
					textcoords='offset points')
				# plots.set_xticklabels(plots.get_xticklabels(), rotation=90, horizontalalignment='right')
				plt.xticks(y_pos, global_gens, rotation = 'vertical')

				# plt.legend(labels = ['Counts of Genes'])
				plt.xlabel('AMR Genes')
				plt.ylabel('Total Genes - Tool: AMRFINDERPLUS')
				plt.savefig(abs_path + '/results/amr_finder_plots/AB_genes_bar_amrfinder.png',dpi=300)
				plt.close()

				print("\nplotting Clustered Heatmap..")
				sns.clustermap(Matrix, cbar = False, cmap = "viridis", figsize= (20,30))
				plt.savefig(abs_path + "/results/amr_finder_plots/AB_genes_cluster_amrfinder.png", dpi = 300)
				plt.close()

			Matrix = pd.DataFrame(Matrix, columns = global_gens, index = name)
			plots(Matrix, gen_counter)


			def generate_antibiotic_count(path, data):
				# print(len(all_gene_names))
				print("\ngenerate AB Cluster ..")
				antibiotic_classnames = []
				antibiotic_counter = []
				mr_isolates_in_class  = []
				for elem in data:
					file = open(path+elem)
					df = pd.read_csv(file, delimiter = '\t', encoding = 'uft-8')
					df['Class'] = df['Class'].fillna('nan')
					for i in range(df["Class"].count()):
						if df.iloc[i]["Class"] not in antibiotic_classnames:
							antibiotic_classnames.append(str(df.iloc[i]["Class"]))
							antibiotic_counter.append(1)
							mr_isolates_in_class.append(0)
						elif df.iloc[i]["Class"] in antibiotic_classnames:
							index = antibiotic_classnames.index(df.iloc[i]["Class"])
							antibiotic_counter[index] += 1
							if df["Class"].count() > 1:
								mr_isolates_in_class[index] += 1

				return antibiotic_classnames, antibiotic_counter, mr_isolates_in_class

			antibiotic_classnames, antibiotic_counter, num_of_mr_isoltes_in_class = generate_antibiotic_count(path, data)




			def antibiotic_cluster(data, path, AB_classnames, AB_Counter, AB_MultiResistance):
				sns.set(style="whitegrid", color_codes=True)
				plt.subplots(figsize=(12,12))
				plots = sns.barplot(x = AB_classnames, y = AB_Counter, palette = colors_from_values(np.array(AB_Counter), "YlOrRd"))
				for bar in plots.patches:
					plots.annotate(format(bar.get_height(), '.2f'),
					(bar.get_x() + bar.get_width() / 2,
					bar.get_height()), ha='center', va='center',
					size=15, xytext=(0, 5),
					textcoords='offset points')
				plots.set_xticklabels(plots.get_xticklabels(), rotation=90, horizontalalignment='right')
				plt.savefig(os.path.abspath(os.getcwd()) + "/results/amr_finder_plots/AB_class_bar_amrfinder.png", dpi=300, bbox_inches="tight")
				plt.close()

			antibiotic_cluster(data, path, antibiotic_classnames, antibiotic_counter, num_of_mr_isoltes_in_class)

			def AB_heatmap(AB_Classnames, data, path, name):
				sns.set(style="whitegrid", color_codes=True)
				print(AB_Classnames)
				print("\nplotting AB_Heatmap ..")
				abs_path = os.path.abspath(os.getcwd())
				AB_Matrix = []
				count = len(AB_Classnames)
				for elem in data:
					file = open(path+elem)
					df = pd.read_csv(file, delimiter = '\t', encoding = "uft-8")
					df['Class'] = df['Class'].fillna('nan')
					iso_spalte = [0 for i in range(count)]
					for i in range(df["Class"].count()):
						index = AB_Classnames.index(df.iloc[i]["Class"])
						iso_spalte[index] += 1
					AB_Matrix.append(iso_spalte)

				AB_Matrix = pd.DataFrame(AB_Matrix, columns = AB_Classnames, index = name)
				AB_Matrix.to_csv(abs_path + "/results/amrfinder_AB_class_counts.csv", sep = ",")
				plt.subplots(figsize=(20,30))
				sns.heatmap(AB_Matrix, cmap="viridis",  cbar = True )
				plt.savefig(abs_path + "/results/amr_finder_plots/AB_heatmap_amrfinder.png", dpi=300)
				plt.close()


			AB_heatmap(antibiotic_classnames, data, path, name)

			def matrix_to_csv(Matrix):
				Matrix = pd.DataFrame(Matrix, columns = global_gens, index = name)
				Matrix.to_csv(os.path.abspath(os.getcwd())+'/results/amrfinder_presence_absence.csv', sep = ",")

			matrix_to_csv(Matrix)
			print("\nFINESHED AMR FINDER PLUS ")
