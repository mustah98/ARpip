import os
import csv
import pandas as pd
import matplotlib as mp
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from pathlib import Path
import warnings
import sys


def amr_tool():
    if config["amr_tool"]["abricate"] == True:
        return(expand("results/abricate_plots/{Dataset}.png", Dataset = DATASET_ABRICATE),
        "results/abricate_presence_absence.csv")
    else:
        return 'results/none.txt'

def input():
    if config["amr_tool"]["abricate"] == True:
        return (os.path.abspath(os.getcwd()) + "/results/abricate/")
    else:
        return 'results/none.txt'


def db():
    if config["abricate"]["database"] == "resfinder":
        return("python3 db_scripts/resfinder.py")
    if config["abricate"]["database"] == "megares":
        return("python3 db_scripts/megares.py")
    if config["abricate"]["database"] == "ncbi":
        return("python3 db_scripts/ncbi.py")
    if config["abricate"]["database"] == "card":
        return("python3 db_scripts/card.py")



rule plot_abricate:
    input:
        expand("results/abricate/{sample}.tsv", sample = list(samples.index))
    output:
        amr_tool(),
    params:
        path = input()
    conda:
        "../envs/env_abricate_results.yaml"
    shell:
        db()
