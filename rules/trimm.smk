rule trimm:
    input:
        r1 = lambda wildcards:samples.at[wildcards.sample, "fq1"] if wildcards.sample in samples.index else '',
        r2 = lambda wildcards:samples.at[wildcards.sample, "fq2"] if wildcards.sample in samples.index else ''
    output:
        r1 = "results/trimm/{sample}_1.fastq",
        r2 = "results/trimm/{sample}_2.fastq",
        html = "results/trimm/{sample}.html",
        json = "results/trimm/{sample}.json",
    params:
        trimmer = config["trimm_params"]
    threads: 4
    conda:
        "../envs/env_trimm.yaml"
    shell:
        "fastp -t {threads} {params.trimmer} -i {input.r1} -I {input.r2} -o {output.r1} -O {output.r2} -h {output.html} -j {output.json}"
