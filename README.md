**ARpip**
-
**An automated and scalable Snakemake pipeline for the detection of genotypic antibiotic resistance in bacteria**


**Instructions for ARpip**
-

Dependencies:
  - Install conda and create an enviroment with python>=3.7 and snakemake>=5.3.0 --> `conda create -n <NAME> python=3.7`
  - Make sure every other enviroment is deactivated --> `conda deactivate`
  - Activate the enviroment --> `conda activate <PATH/TO/ENV>` or `conda activate <NAME>`
  - Install Snakemake --> `conda install -c bioconda snakemake`
  - Recommendation: you can download snakemake via mamba to get up-to-date versions
  - Pipeline downloads Python librarys automaticly if library is nessesary but not available 
  
Input:
  - The pipeline can process Paired-End (PE) Reads (.fastq and .fastq.zip) and whole-genome-assmeblys (.fasta and .fasta.zip)
  - Inputs should be inside a clean dir with ONLY PE-read or assemblys
  - Configure  the configfile at `config['data']` with the path to the input files or use --> `snakemake --config data='<PATH/TO/DATA/>`
  - All parameters that can be set up for the analysis are marked with "#" in the configfiles 
  - If parameter is not labeled as #, DO NOT change it 

Usage:
  - Run the Pipeline with the following command within the directory where the `Snakefile` is located --> `python3 ARpip.py -t <AMOUNT OF CORES> -p <PATH TO DATA>`
  - The first run will take some time. In this the enviroments for each rule are build
  - If no thread amount was specified, a default of 4 threads will be used
Output
  - The final pipeline-output consists of figures and a presence-absence gene file from each respective tools

Config File:
  - With the help of the config file it is possible to configure some analysis steps.
  - Quality control: 
	- Here it should be specified in which steps of the analysis a quality control of the data should be made.
	- The respective steps are activated or deactivated with "True" or "False".
  - Abricate:
	- If Abricate is to be used for AR detection, the desired database must be specified here. The following options are available:
		- ncbi - megares - card - resfinder
	- In addition, the coverage and identity should be specified using int-values
  - AMRfinder:
	- If AMRfidner is to be used for the AR detection, the identity and coverage should be specified as strings
  - Annotation:
	- If an annotation is required, it should be specified as "True". And if so, the kingome and genus of the organism should be specified for the Tool Prokka
	

