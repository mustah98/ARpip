import unittest
import testskript
import os
import csv
import pandas as pd
import matplotlib.pyplot as plt
import sys
import numpy as np
import seaborn as sns

modules_dir = os.path.dirname(os.path.abspath(testskript.__file__))
print(modules_dir)
data_dir = os.path.join(modules_dir, 'test_sample/')
print(data_dir)
class GlobalGenes(unittest.TestCase):
    def test__find_global_genes(self):
        expected_output = ['gyrA_T86I', 'tet(O)', 'blaOXA-184', 'arsP', 'acr3', 'blaOXA-193', '50S_L22_A103V']
        path = data_dir
        data = sorted(os.listdir(path))
        global_gens = []
        got = testskript.find_global_genes(data, path, global_gens)
        print(expected_output, '\n', got)
        self.assertEqual(expected_output , got)

    def test__find_gene(self):
        expected_output = [[1, 1, 1 ,1 , 1, 0, 0], [0, 0, 0, 1, 0, 1, 0], [0, 0, 0, 1, 0, 0, 1]]
        Matrix = [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]]
        path = data_dir
        data = sorted(os.listdir(path))
        global_gene = testskript.find_global_genes(data, path, global_gens = [])
        got = testskript.find_gene(Matrix, data, path, global_gene)
        print(got, "\n", expected_output)
        self.assertEqual(expected_output, got)

    def test__get_num_of_genes(self):
        expected_output = [1, 1, 1, 3, 1, 1, 1]
        Matrix = [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]]
        path = data_dir
        data = sorted(os.listdir(path))
        global_gene = testskript.find_global_genes(data, path, [])
        Matrix = testskript.find_gene(Matrix, data, path, global_gene)
        got = testskript.get_num_of_gen(7, 3, [], Matrix)
        print(expected_output, '\n', got)
        self.assertEqual(expected_output, got)
