install.packages("pheatmap")
library(pheatmap)

metadata = read.table("/scratch1/mustafa/Data/internship_amr/Pipeline_ready/stuff/annotation.tsv", header = T, sep = "\t", row.name = 1)
df_abricate = read.csv('results/abricate_presence_absence.csv', na.strings = "NA", header=T, row.names = 1)
df_abricate = df_abricate[apply(df_abricate[,-1], 1, function(x) !all(x==0)),]
print(df_abricate)
print(metadata)

df_amrfinder = read.csv('results/amrfinder_presence_absence.csv', na.strings = "NA", header=T, row.names = 1)
df_amrfinder = df_amrfinder[apply(df_amrfinder[,-1], 1, function(x) !all(x==0)),]
# print(df_amrfinder)
png(file = 'results/abricate_R_heatmap.png', width=6000, height=10000, res=500)
pheatmap(mat = df_abricate,main = "abricate_presence_absence", cluster_rows = TRUE, cluster_cols = TRUE,border_color = "grey60",  width=1500, height=1500, res=500, fontsize = 5, annotation_row = metadata)

dev.off()
# rownames(df_abricate) = df_abricate$isolate
# dev.off()
png(file = 'results/amrfinder_R_heatmap.png',  width=6000, height=10000, res=500)
pheatmap(df_amrfinder,main = "amrfinder_presence_absence",cluster_rows = TRUE, cluster_cols = TRUE, border_color = "grey60", width=6000, height=6000, res=500,fontsize = 5, annotation_row = metadata)
dev.off()
